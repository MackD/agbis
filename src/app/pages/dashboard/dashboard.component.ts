import {Component, OnInit} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {UserService} from '@app/_services';
import {NgxSpinnerService} from 'ngx-spinner';

export enum Waiting {
  'не требует подтверждения',
  'требует подтверждения',
  'подтвержден',
  'отменен'
}

export enum Kind {
    'химчистка',
    'прачечный',
    'товар',
    'выезд'
}

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    dataUserInfo = {};
    dataUserOrders = [];
    dataOrderInfo = [];
    waiting = Waiting;
    kind = Kind;
    private idOrder: number;

    constructor(
        private _userService: UserService,
        private spinner: NgxSpinnerService,
        private modalService: NgbModal) {
    }

// todo Add toastr
    ngOnInit() {
        this._userService.getUserInfo().subscribe({
                next: ({error, ...dataUserInfo}) => {
                    this.spinner.show();
                    console.log({dataUserInfo});
                    this.dataUserInfo = dataUserInfo;
                },
                error: () => {
                    this.spinner.hide();
                },
                complete: () => {
                    this.spinner.hide()
                }
            });

        this._userService.getUserOrders().subscribe({
          next: ({error, orders}) => {
            this.spinner.show('orders');
            console.log({orders});
            this.dataUserOrders = orders;
          },
          error: () => {
            this.spinner.hide('orders');
          },
          complete: () => {
            this.spinner.hide('orders');
          }
        });
    }

    public getOrderInfo(id:number, modalContent: any){
        this.spinner.show();
        this._userService.getUserOrderService(id).subscribe({
            next: ({error, order_services}) => {
                console.log({order_services});
                this.dataOrderInfo = order_services;
                this.idOrder = id;
            },
            error: () => {
                this.spinner.hide();
            },
            complete: () => {
                this.spinner.hide();
                this.modalService.open(modalContent, { scrollable: true, size: 'xl' });
            }
        });
    }
    // sort for json
    asIs = () => 0;
}
