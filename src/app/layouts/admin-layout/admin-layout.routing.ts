import { Routes } from '@angular/router';

import { DashboardComponent } from '@app/pages/dashboard/dashboard.component';
import { IconsComponent } from '@app/pages/icons/icons.component';
import { MapsComponent } from '@app/pages/maps/maps.component';
import { UserProfileComponent } from '@app/pages/user-profile/user-profile.component';
import { TablesComponent } from '@app/pages/tables/tables.component';

export const AdminLayoutRoutes: Routes = [
    { path: '',      component: DashboardComponent },
    { path: 'user-profile',   component: UserProfileComponent },
    { path: 'tables',         component: TablesComponent },
    { path: 'icons',          component: IconsComponent },
    { path: 'maps',           component: MapsComponent }
];
