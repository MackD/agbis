import {Component, OnInit} from '@angular/core';
import {UserService, DataService} from '@app/_services';

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.scss']
})
export class AdminLayoutComponent implements OnInit {
    userData: any;
  constructor(private _userService: UserService, private _dataService: DataService) {
  }

  ngOnInit() {

  }

}
