import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {AuthenticationService} from '@app/_services/authentication.service';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

// @ts-ignore
@Injectable({ providedIn: 'root' })
export class UserService {
    public userData: Observable<any>;
    private readonly userDataSubject: BehaviorSubject<any>;

    constructor(private http: HttpClient, private authenticationService: AuthenticationService) {
        this.userDataSubject = new BehaviorSubject<any>('none');
        this.userData = this.userDataSubject.asObservable();
    }

    private sessionId(){
        const {Session_id} = this.authenticationService.currentUserAuthValue;
        return Session_id
    };

    public get userDataValue(): any {
        return this.userDataSubject.value;
    }
    // todo Create Interface
    getUserInfo() {
        const uri = `/api/?ContrInfo&SessionID=${this.sessionId()}`;
        return this.http.get(encodeURI(uri))
            .pipe(
                map((UserData: any) => {
                    if(UserData.error !== 0)
                        return UserData;
                    this.userDataSubject.next(UserData);
                    return UserData;
                })
            );
    }
    getUserOrders() {
        const uri = `/api/?Orders={"sclad": 1, "need_serv": 0}&SessionID=${this.sessionId()}`;
        return this.http.get(encodeURI(uri))
            .pipe(
                map((UserOrders: any) => {
                    if(UserOrders.error !== 0)
                        return UserOrders;
                    return UserOrders;
                })
            );
    }
    getUserOrderService(id:number) {
        const uri = `/api/?FullService={"dor_id": "${id}"}&SessionID=${this.sessionId()}`;
        return this.http.get(encodeURI(uri))
            .pipe(
                map((UserOrderService: any) => {
                    if(UserOrderService.error !== 0)
                        return UserOrderService;
                    return UserOrderService;
                })
            );
    }
}
