import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable, from} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

import {environment} from 'environments/environment';
import {UserAuth} from '@app/_models';
// @ts-ignore
import * as sha1 from 'sha1';



@Injectable({providedIn: 'root'})
export class AuthenticationService {
    public currentUserAuth: Observable<UserAuth>;
    private currentUserAuthSubject: BehaviorSubject<UserAuth>;

    constructor(private http: HttpClient) {
        this.currentUserAuthSubject = new BehaviorSubject<UserAuth>(JSON.parse(localStorage.getItem('currentUserAuth')));
        this.currentUserAuth = this.currentUserAuthSubject.asObservable();
    }

    public get currentUserAuthValue(): UserAuth {
        return this.currentUserAuthSubject.value;
    }

    login(phone: string, password: string): Observable<any> {
        const uri = `/api/?ModernLogin={"phone": "${phone}", "pwd": "${sha1(password)}"}`;
        return this.http.get(encodeURI(uri),{withCredentials: true})
            .pipe(
                map((authData: any) => {
                    if(authData.error !== 0)
                        return authData;
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    console.log({authData});
                    localStorage.setItem('currentUserAuth', JSON.stringify(authData));
                    this.currentUserAuthSubject.next(authData);
                    return authData;
                })
                );
    }


    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUserAuth');
        this.currentUserAuthSubject.next(null);
    }
}
